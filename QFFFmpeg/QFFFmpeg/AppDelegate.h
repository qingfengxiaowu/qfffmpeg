//
//  AppDelegate.h
//  QFFFmpeg
//
//  Created by Feng Hongen on 2017/8/10.
//  Copyright © 2017年 Feng Hongen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

