//
//  QFMultimedia.m
//  IJKMediaPlayer
//
//  Created by Qingfeng on 2017/7/17.
//  Copyright © 2017年 bilibili. All rights reserved.
//

#import "QFMultimedia.h"
#import "ffmpeg.h"

static QFMultimediaProgress multimediaProgress = nil;

static QFMultimediaResult multimediaResult = nil;

void qf_multimedia_progress(double transcoding_time) {
    !multimediaProgress ?: multimediaProgress(transcoding_time);
}

void qf_multimedia_result(int ret) {
    !multimediaResult ?: multimediaResult(0 == ret);
}

@implementation QFMultimedia

+ (int)multimediaRunWithArguments:(NSArray *)arguments progress:(QFMultimediaProgress)progress {
    return 1;
}

+ (int)qf_multimedia_run:(int)argumentCount
               arguments:(char **)arguments
                progress:(QFMultimediaProgress)progress
              completion:(QFMultimediaResult)completion {
    multimediaProgress = progress;
    multimediaResult = completion;
    return ffmpeg_main(argumentCount, arguments, qf_multimedia_progress, qf_multimedia_result);
}

@end
