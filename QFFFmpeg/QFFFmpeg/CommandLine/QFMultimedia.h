//
//  QFMultimedia.h
//  IJKMediaPlayer
//
//  Created by Qingfeng on 2017/7/17.
//  Copyright © 2017年 bilibili. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^QFMultimediaProgress)(double transcodingTime);

typedef void(^QFMultimediaResult)(BOOL success);

@interface QFMultimedia : NSObject

+ (int)multimediaRunWithArguments:(NSArray *)arguments progress:(QFMultimediaProgress)progress;

+ (int)qf_multimedia_run:(int)argumentCount
               arguments:(char **)arguments
                progress:(QFMultimediaProgress)progress
              completion:(QFMultimediaResult)completion;

@end
