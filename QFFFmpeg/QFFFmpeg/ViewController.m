//
//  ViewController.m
//  QFMultimedia
//
//  Created by Qingfeng on 2017/7/4.
//  Copyright © 2017年 Qingfeng. All rights reserved.
//

#import "ViewController.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "transcoding.h"
#import "QFMultimedia.h"

#define DocumentDir [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]
#define BundlePath(res) [[NSBundle mainBundle] pathForResource:res ofType:nil]
#define DocumentPath(res) [DocumentDir stringByAppendingPathComponent:res]

@interface ViewController ()

@property (nonatomic) NSInteger videoIndex;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect frame = CGRectMake(60, 200, 220, 50);
    UIButton *padButton = [UIButton buttonWithType:UIButtonTypeCustom];
    padButton.frame = frame;
    padButton.exclusiveTouch = YES;
    padButton.titleLabel.font = [UIFont systemFontOfSize:20];
    [padButton setTitle:@"CMTranscode Video" forState:UIControlStateNormal];
    [padButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [padButton addTarget:self
                  action:@selector(didTapPadButton:)
        forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:padButton];
    
    frame.origin.y += 100;
    UIButton *filterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    filterButton.frame = frame;
    filterButton.exclusiveTouch = YES;
    filterButton.titleLabel.font = [UIFont systemFontOfSize:20];
    [filterButton setTitle:@"Filter Video" forState:UIControlStateNormal];
    [filterButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [filterButton addTarget:self
                     action:@selector(didTapFilterButton:)
           forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:filterButton];
    
    frame.origin.y += 100;
    UIButton *playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    playButton.frame = frame;
    playButton.exclusiveTouch = YES;
    playButton.titleLabel.font = [UIFont systemFontOfSize:20];
    [playButton setTitle:@"Play Video" forState:UIControlStateNormal];
    [playButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [playButton addTarget:self
                   action:@selector(didTapPlayButton:)
         forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:playButton];
}

- (void)didTapPadButton:(UIButton *)button {
    [self cmTranscodeVideo];
}

- (void)didTapFilterButton:(UIButton *)button {
    [self customTranscodeVideo];
}

- (void)didTapPlayButton:(UIButton *)button {
    NSString *outputVideoName = @"custom_transcoding_video.mp4";
    NSString *videoPath = DocumentPath(outputVideoName);
    if (![[NSFileManager defaultManager] fileExistsAtPath:videoPath]) {
        return;
    }
    
    // videoPath = BundlePath(@"2.mp4");
    NSURL *videoURL = [NSURL fileURLWithPath:videoPath];
    AVPlayer *player = [AVPlayer playerWithURL:videoURL];
    AVPlayerViewController *playerViewController = [AVPlayerViewController new];
    playerViewController.player = player;
    [self presentViewController:playerViewController
                       animated:YES
                     completion:^{
                         [player play];
                     }];
}

- (void)customTranscodeVideo {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *outputVideoName = @"custom_transcoding_video.mp4";
        NSString *outputVideoPath = DocumentPath(outputVideoName);
        char *outVideoPath = (char *)[outputVideoPath UTF8String];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:outputVideoPath]) {
            [fileManager removeItemAtPath:outputVideoPath error:nil];
        }
        char *sourceVideoPath = (char *)[BundlePath(@"IMG_4153.MOV") UTF8String];
        char *arguments[] = {
            "transcode",
            sourceVideoPath,
            outVideoPath
        };
        transcoding_main(sizeof(arguments)/sizeof(*arguments), arguments);
    });
}

- (void)cmTranscodeVideo {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *outputVideoName = @"cm_transcode.mp4";
        NSString *outputVideoPath = DocumentPath(outputVideoName);
        char *outVideoPath = (char *)[outputVideoPath UTF8String];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:outputVideoPath]) {
            [fileManager removeItemAtPath:outputVideoPath error:nil];
        }
        char *sourceVideoPath = (char *)[BundlePath(@"12344.mp4") UTF8String];
        char *arguments[] = {
            "ffmpeg",
            "-y",
            "-i",
            sourceVideoPath,
            "-vf",
            "scale=720:406",
            "-b:v",
            "4500k",
            outVideoPath
        };
        [QFMultimedia qf_multimedia_run:sizeof(arguments)/sizeof(*arguments)
                              arguments:arguments
                               progress:^(double transcodingTime) {
                                   NSLog(@"=====filter_time===:%f", transcodingTime);
                               }
                             completion:^(BOOL success) {
                                 NSLog(@"=====filter_result===:%ld", (long)success);
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     NSURL *videoURL = [NSURL fileURLWithPath:outputVideoPath];
                                     AVPlayer *player = [AVPlayer playerWithURL:videoURL];
                                     AVPlayerViewController *playerViewController = [AVPlayerViewController new];
                                     playerViewController.player = player;
                                     [self presentViewController:playerViewController
                                                        animated:YES
                                                      completion:^{
                                                          [player play];
                                                      }];
                                 });
                             }];
    });
}

- (void)padVideo {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *outputVideoName = @"pad_video.mp4";
        NSString *outputVideoPath = DocumentPath(outputVideoName);
        char *outVideoPath = (char *)[outputVideoPath UTF8String];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:outputVideoPath]) {
            [fileManager removeItemAtPath:outputVideoPath error:nil];
        }
        char *sourceVideoPath = (char *)[BundlePath(@"2.mp4") UTF8String];
        char *arguments[] = {
            "ffmpeg",
            "-i",
            sourceVideoPath,
            "-vf",
            "pad=640:1138:0:0:black",
            "-b:v",
            "1000k",
            outVideoPath
        };
        [QFMultimedia qf_multimedia_run:sizeof(arguments)/sizeof(*arguments)
                              arguments:arguments
                               progress:^(double transcodingTime) {
                                   NSLog(@"=====filter_time===:%f", transcodingTime);
                               }
                             completion:^(BOOL success) {
                                 NSLog(@"=====filter_result===:%ld", (long)success);
                                 
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     NSURL *videoURL = [NSURL fileURLWithPath:outputVideoPath];
                                     AVPlayer *player = [AVPlayer playerWithURL:videoURL];
                                     AVPlayerViewController *playerViewController = [AVPlayerViewController new];
                                     playerViewController.player = player;
                                     [self presentViewController:playerViewController
                                                        animated:YES
                                                      completion:^{
                                                          [player play];
                                                      }];
                                 });
                             }];
    });
}

@end
